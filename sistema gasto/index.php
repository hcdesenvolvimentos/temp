

<!DOCTYPE html>
<html lang="pt-br">
<!-- PHP CONEXÃO -->
<?php include ('conexao_banco.php'); ?>

<head>

	<!-- META -->
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- TÍTULO -->
	<title>Gasto Hudson Gabriel</title>

	<!-- FONTS -->
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,600,600italic,700,700italic,300italic' rel='stylesheet' type='text/css'>

	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
	<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css" />
	<link rel="stylesheet" href="css/site.css" />

	<!-- JS -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/geral.js"></script>
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/jquery.maskedinput.js"></script>

	<!-- FAVICON -->
	<link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico" />

</head>
<body>

<div class="row titulo">
	<div class="col-md-12">Gastos do Mês</div>
</div>
<div class="container">
	<ul class="menu">
		<li><a href="#" style="background-color:#000">Ínicio</a></li>
		
		<li><a href="relatorio.php">Relatório</a></li>
		
	</ul>
</div>	

	<div class="container">
		<div class="row">
			<div class="col-md-6">

				
				<!-- LOOP PARA RECEBER DADOS CADASTRADOS  INSERIR NO BANCO-->
				<?php  
					// CONDIÇÃO PARA PEGAR CLICK DO BOTÃO 
					if (@$_REQUEST['botao'] == "Guardar") 
					{
						// INSERÇÃO NAS TABELAS
						$insere = "INSERT into itens (nome,valor,data,descricao) VALUES ('{$_POST['nome']}', '{$_POST['valor']}', '{$_POST['data']}', '{$_POST['descricao']}')";
						$result_insere = mysql_query($insere);
						

						if ($result_insere) echo "<h2> Registro inserido com sucesso!!!</h2>";
						else echo "<h2> Nao consegui inserir!!!</h2>";
						
						if( $result_insere ){
						  header( 'Location: index.php' );
						}

					}	else

					// CONDIÇÃO PARA PEGAR CLICK DO BOTÃO 
					 if (@$_REQUEST['adicionar'] == "Adicionar") {

				 		// INSERÇÃO NAS TABELAS
						$insere = "INSERT into intems_adicionados (intens_adicionais) VALUES ('{$_POST['intens_adicionais']}')";
						$result_insere = mysql_query($insere);
						
						if ($result_insere) echo "<h2> Registro inserido com sucesso!!!</h2>";
						else echo "<h2> Nao consegui inserir!!!</h2>";
						
						if( $result_insere ){
						  header( 'Location: index.php' );
						}

					}


				?>

				<!-- INSERINDO NOVO ITEM -->
				<div class="form">
					<form action="index.php" method="post" name="index.php">

						<span>

							<label for="name">Adicionar Novo Item</label>
		        			<input type="text" id="novo-item" class="item" placeholder="Novo Item" required="required" name="intens_adicionais" />
							<input type="submit" value="Adicionar" name="adicionar">
				
						</span>

					</form>
				</div>
					
					<hr>

				<div class="form">
					<form action="index.php" method="post" name="index.php">

						
						<!-- SELECT DE ITEMS  -->
						<div class="form-group select">
					    	<label for="sel1">Selecione</label>
							
							<!-- PEGANDO ITEMS ADICIONADOS NA TABELA  -->
							<?php
								$query = "	SELECT intens_adicionais, intens_adicionais	FROM intems_adicionados	ORDER BY intens_adicionais	";
								$result = mysql_query($query);	
							?>

				            <!-- SELECT -->
				            <select  name="nome" required="required"  >
					            <option value="">Selecione o item</option>
					            
					            <!-- LOOP PARA MOSTRAR ITEMS DA TABELA  DO BANCO DE DADOS -->
					            <?php
									while( $row = mysql_fetch_assoc($result) )
									{
								?>
							           
							            <option value="<?php echo $row['intens_adicionais']; ?>" ><?php echo @$row['intens_adicionais'] ?></option>
					            
					            <?php
									}
								?>

				          	</select>
						</div> 
						
						
						<!-- INPUT PATA RECEBER OS VALORES PARA SER AMRMAZENADOS NO BANCO  -->
						<span>
							<label for="name">Valor:R$</label>
		        			<input type="float" id="valor" class="valor" required="required"  placeholder="R$ 00.00" name="valor" />
						</span>

						<span>
							<label for="name">Data:</label>
		        			<input type="date" id="data" placeholder="0000/00/00" class="data" name="data" />
						</span>

						<span>
							<label for="Descrição">Descrição</label>
							<textarea name="descricao" id="Descrição" cols="30" rows="10"></textarea>
						</span>
						
						<span>	
							<input type="submit" id="botao" name="botao" value="Guardar">
							<input type="reset" id="botao" name="novo" value="Novo">
						</span>

					</form>
				</div>

			</div>

			<div class="col-md-6">
				<h2><i>Instruções</i> </h2>
				<a href="#" title=""  class="links" data-toggle="collapse" data-target="#demo">Adicionar um novo item  ! <i class="fa fa-angle-down"></i></a>
				<a href="#" title="" class="links"  data-toggle="collapse" data-target="#demo1">Selecionar um novo item  ! <i class="fa fa-angle-down"></i></a>
				<a href="#" title=""  class="links" data-toggle="collapse" data-target="#demo2">Atribuindo um valor para o item adicionado  ! <i class="fa fa-angle-down"></i></a>
				<a href="#" title=""  class="links" data-toggle="collapse" data-target="#demo3">Atribuindo uma data para o item adicionado  ! <i class="fa fa-angle-down"></i></a>
				<a href="#" title=""  class="links" data-toggle="collapse" data-target="#demo4">Atribuindo uma descrição para o item adicionado  ! <i class="fa fa-angle-down"></i></a>


				<!-- DETALHES -->
				<div  id="demo" class="collapse bloco">
					
					<!-- CONTEÚDO -->
					
					<div class="row instrucao">
						<div class="col-md-6 instrucao"><img src="img/img1.jpg" alt=""></div>
					</div>

					<div class="row instrucao">
						<div class="col-md-6 instrucao">
							
							<p>Você pode adicionar um novo item não existente no sistema.</p>

						</div>
					</div>
					
				</div>

				<!-- DETALHES -->
				<div id="demo1" class="collapse bloco">
					
					<!-- CONTEÚDO -->
					
					<div class="row instrucao">
						<div class="col-md-6 instrucao"><img src="img/img2.jpg" alt=""></div>
					</div>

					<div class="row instrucao">
						<div class="col-md-6 instrucao">
							
							<p>Você pode selecionar um novo item já existente no sistema ou adicionado ao msm tempo .</p>

						</div>
					</div>
					
				</div>
					
				<!-- DETALHES -->
				<div id="demo2" class="collapse bloco">
					
					<!-- CONTEÚDO -->
					
					<div class="row instrucao">
						<div class="col-md-6 instrucao"><img src="img/img3.jpg" alt=""></div>
					</div>

					<div class="row instrucao">
						<div class="col-md-6 instrucao">
							
							<p>Você pode atribuir um valor para o  item que será adicionado ao sistema exemplo:<i>R$ 10.40</i>.</p>

						</div>
					</div>
					
				</div>
				
				<!-- DETALHES -->
				<div id="demo3" class="collapse bloco">
					
					<!-- CONTEÚDO -->
					
					<div class="row instrucao">
						<div class="col-md-6 instrucao"><img src="img/img4.jpg" alt=""></div>
					</div>

					<div class="row instrucao">
						<div class="col-md-6 instrucao">
							
							<p>Você pode atribuir uma data de quando o item foi adicionado ao sistema exemplo:<i>2015/10/26 </i>.</p>

						</div>
					</div>
					
				</div>

				<!-- DETALHES -->
				<div id="demo4" class="collapse bloco">
					
					<!-- CONTEÚDO -->
					
					<div class="row instrucao">
						<div class="col-md-6 instrucao"><img src="img/img5.jpg" alt=""></div>
					</div>

					<div class="row instrucao">
						<div class="col-md-6 instrucao">
							
							<p>Você pode atribuir uma descrição ao item adicionado o sistema.</p>

						</div>
					</div>
					
				</div>

			</div>

		</div>
	</div>
	
	<!-- SCRIPT MASCARA DO INPUT DATA -->
	<script>
		jQuery(function($){
			$("#data").mask("99/99/9999");
		});
	</script>

</html>
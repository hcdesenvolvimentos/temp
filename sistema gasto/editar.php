<!DOCTYPE html>
<html lang="pt-br">
<!-- PHP CONEXÃO -->
<?php include ('conexao_banco.php'); ?>

<head>

	<!-- META -->
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- TÍTULO -->
	<title>Gasto Hudson Gabriel</title>

	<!-- FONTS -->
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,600,600italic,700,700italic,300italic' rel='stylesheet' type='text/css'>

	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
	<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css" />
	<link rel="stylesheet" href="css/site.css" />

	<!-- JS -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/geral.js"></script>
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/jquery.maskedinput.js"></script>

	<!-- FAVICON -->
	<link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico" />

</head>
<body>

<div class="row titulo">
	<div class="col-md-12">Atualizando item do Mês</div>
</div>
<div class="container">
	<ul class="menu">
		<li><a href="index.php">Ínicio</a></li>
		
		<li><a href="relatorio.php">Relatório</a></li>
		
	</ul>
</div>	

	<div class="container">
		<div class="row">
			<div class="col-md-12">
				
				<?php  

					$id = @$_REQUEST['id'];

					if (@$_REQUEST['botao'] == "Excluir") {
							$query_excluir = "
								DELETE FROM itens WHERE id='$id'
							";
							$result_excluir = mysql_query($query_excluir);
							
							if ($result_excluir) echo "<h2><strong>Seu item foi excluido </strong> do sistema com sucesso!!!</h2>";
							else echo "<h2> Nao consegui excluir!!!</h2>";

							
					}
					
					if (@$_REQUEST['id'] and @!$_REQUEST['botao'])
					{
						$query = "
							SELECT * FROM itens WHERE id='{$_REQUEST['id']}'
						";
						$result = mysql_query($query);
						$row = mysql_fetch_assoc($result);
						//echo "<br> $query";	
						foreach( $row as $key => $value )
						{
							$_POST[$key] = $value;
						}
					}



				// CONDIÇÃO PARA PEGAR CLICK DO BOTÃO 
					if (@$_REQUEST['botao'] == "Guardar") 
					{
						if (!$_REQUEST['id'])
						{
							// INSERÇÃO NAS TABELAS
							$insere = "INSERT into itens (nome,valor,data,descricao) VALUES ('{$_POST['nome']}', '{$_POST['valor']}', '{$_POST['data']}', '{$_POST['descricao']}')";
							$result_insere = mysql_query($insere);
							

							if ($result_insere) echo "<h2> <strong> Seu item foi enviado </strong>para o  sistema com sucesso!!!</h2>";
							else echo "<h2> Nao consegui inserir!!!</h2>";
							
							if( $result_insere ){
							  header( 'Location: index.php' );
							}
						}else 

						{
							$insere = "UPDATE itens SET 
										nome = '{$_POST['nome']}'
										, valor = '{$_POST['valor']}'
										, data = '{$_POST['data']}'
										, descricao = '{$_POST['descricao']}'
										WHERE id = '{$_REQUEST['id']}'
									";
							$result_update = mysql_query($insere);

							if ($result_update) echo "<h2> <strong> Seu item foi atualizado </strong> no sistema com sucesso!!!</h2>";
							else echo "<h2> Nao consegui atualizar!!!</h2>";
							
							
						}
					}

				?>

				<div class="form">
					<form action="editar.php" method="post" name="editar.php">
						<span>
							<label for="nome">Nome</label>
		        			<input type="text" id="nome" class="nome" required="required" value="<?php echo @$_POST['nome']; ?>" name="nome" />
						</span>
						
						<span>
							<label for="name">Valor:R$</label>
		        			<input type="text" id="valor" class="valor" required="required" value="<?php echo @$_POST['valor']; ?>" name="valor" />
						</span>
						<span>
							<label for="name">Data:</label>
		        			<input type="text" id="data" required="required" class="data" name="data" value="<?php echo @$_POST['data']; ?>" />
						</span>

						
						<span>
							<label for="Descrição">Descrição</label>
							<textarea name="descricao" id="Descrição" cols="30" rows="10" ><?php echo @$_POST['descricao']; ?></textarea>
						</span>
						

						
							
							<input type="submit" id="botao" name="botao" value="Guardar">
							<input type="submit" value="Excluir" name="botao">
							<input type="" name="id" value="<?php echo @$_REQUEST['id'] ?>" />
						</span>

					</form>
				</div>

					
			</div>
		</div>
	</div>
	
	<script>
		jQuery(function($){
			$("#data").mask("99/99/9999");
		});
	</script>

</body>
</html>